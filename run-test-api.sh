#!/bin/sh

. ./update-manager-common.sh

phase_boot()
{
	prepare_outdated_commit
}

phase_clean_previous_install()
{
	clean_previous_install
}

phase_update()
{
	ostree admin status

	DELTAFILE=$(get_static_delta "")

	apply_update_sync -d $DELTAFILE

	check_current_status "$STATUS_PENDING"
}

phase_check_update()
{
	ostree admin status

	local PHASE_DATA=$(get_phase_data_path)
	local COMMIT_BEFORE=$(cat $PHASE_DATA)

	# The current commit after the update
	local COMMIT_AFTER="$(ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p')"

	if [ "$COMMIT_BEFORE" = "$COMMIT_AFTER" ] ; then
		echo "The update did not apply, the current commit is the same"
		exit 1
	fi
}

phase_update_mount_detect()
{
	ostree admin status

	local DELTAFILE=$(get_static_delta "" "" ".enc")

	# Create a big enough image file
	mkfs.ext4 /var/image_file 400000

	local LOOPDEV=$(losetup -Pf --show /var/image_file)

	# Mount the image file and copy the update
	mkdir /var/mn
	mount $LOOPDEV /var/mn
	cp $DELTAFILE /var/mn/static-update.bundle.enc
	umount /var/mn
	rmdir /var/mn

	# Remount the image using udisksctl to get the notification in aum
	local BEFORE_UP_DATE="$(date '+%Y-%m-%d %H:%M:%S')" # format of the dates for systemd 2012-10-30 18:17:16

	## Start the handler in nohup because LAVA waits for all processes to terminate (like ssh)
	# and if the handler quits, AUM reboots. And if AUM reboots, LAVA timeouts.
	nohup updatectl --register-upgrade-handler & UPGRADEHANDLERPID=$!

	# Start a journal in parallel for logging
	journalctl --since "${BEFORE_UP_DATE}" --unit apertis-update-manager -ef & JOURNALPID=$!

	udisksctl mount -b $LOOPDEV

	# the update should have been detected
	sleep 60

	if journalctl --since "${BEFORE_UP_DATE}" --unit apertis-update-manager | grep -qE "Ostree upgrade ready, system should be rebooted" ; then
		echo "Update occured"
	else
		echo "Automatic update failed"
	fi

	# kill the upgrade handler
	# Requires update-manager 0.1812.1 to avoid reboot when
	# the handler disconnects
	kill $UPGRADEHANDLERPID

	# Terminate journal logging
	kill $JOURNALPID
}

update_manager_phases\
	phase_boot\
	phase_clean_previous_install\
	phase_update\
	phase_check_update\
	phase_boot\
	phase_clean_previous_install\
	phase_update_mount_detect\
	phase_check_update
