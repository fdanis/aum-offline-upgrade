#!/bin/sh

. ./update-manager-common.sh

phase_boot()
{
	prepare_outdated_commit
}

phase_clean_previous_install()
{
	clean_previous_install
}

phase_update()
{
	ostree admin status

	apply_update_sync -o

	check_current_status "$STATUS_PENDING"

	# Check the bootcounter state
	# It may have different boot count on different boards due LAVA
	check_bootcounter "" "" "" 01
}

phase_check_update()
{
	ostree admin status

	local PHASE_DATA=$(get_phase_data_path)
	local COMMIT_BEFORE=$(cat $PHASE_DATA)

	# The current commit after the update
	local COMMIT_AFTER="$(ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p')"

	# NB: may be upgraded to the newer version due the long queue in LAVA
	if [ "$COMMIT_BEFORE" = "$COMMIT_AFTER" ] ; then
		echo "The update did not apply, the current commit is the same"
		exit 1
	fi

	# Ensure to have non-upgrade state
	check_bootcounter "" "" 00 00
}

phase_corrupt_mark()
{
	ostree admin status

	AUCDIR=/etc/systemd/system/apertis-update-complete.service.d
	mkdir -p "${AUCDIR}"
	# Override the update mark service to failing for next boots
	tee "${AUCDIR}"/override.conf <<E_O_F
[Service]
ExecStart=
ExecStart=/bin/false
E_O_F
	systemctl daemon-reload
}

# Check if the system is not reverted after successful upgrade.
# Need to repeat several times to be sure if the bootloader
# doesn't trigger the update revert by error.
phase_check_failed()
{
	# Check if the system is still "updated"
	phase_check_update

	# Check if we are not able to mark the system with success
	# and rewrite the boot counter state from bootloader
	if systemctl status apertis-update-complete.service ; then
		echo "Error: the system is able to mark update as successful"
		exit 1
	fi
}

update_manager_phases\
	phase_boot\
	phase_clean_previous_install\
	phase_update\
	phase_check_update\
	phase_corrupt_mark\
	phase_check_failed\
	phase_check_failed\
	phase_check_failed\
	phase_check_failed
