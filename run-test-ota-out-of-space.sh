#!/bin/sh

. ./update-manager-common.sh

phase_boot()
{
	prepare_outdated_commit
}

phase_clean_previous_install()
{
	clean_previous_install
}

phase_test_update_does_not_start_if_disk_too_low()
{
	ostree admin status

	# Compute remaining disk space from df output (Note that this does not use Available because there is in fact more space available that advertised)
	local MAGIC=25
	local DISKSPACE=$(busybox df -P -B 1M / | grep / | awk '{ print $2-$3-'$MAGIC' }')

	# Fill the disk
	busybox dd if=/dev/zero of=/var/bigfile bs=1M count=$DISKSPACE

	# Ensure the update did not apply
	ret=0
	apply_update_sync -o || ret=$?
	case "$ret" in
		0)
			echo "Update applied. It should not have applied."
			exit 1
			;;
		1)
			echo "Update failed as expected."
			;;
		*)
			echo "Internal bug detected."
			exit 1
			;;
	esac

	rm -f /var/bigfile
}

phase_update_stops_safely_if_disk_too_low()
{
	# Ensure ostree does not prevent the update to start
	ostree config set core.min-free-space-percent "0"
	systemctl restart apertis-update-manager

	# Fill the disk
	local MAGIC=25
	local DISKSPACE=$(busybox df -P -B 1M / | grep / | awk '{ print $2-$3-'$MAGIC' }')
	busybox dd if=/dev/zero of=/var/bigfile bs=1M count=$DISKSPACE

	# Ensure the update did not apply
	ret=0
	apply_update_sync -o || ret=$?
	case "$ret" in
		0)
			echo "Update applied. It should not have applied."
			exit 1
			;;
		1)
			echo "Update failed as expected."
			;;
		*)
			echo "Internal bug detected."
			exit 1
			;;
	esac

	# Ensure the reason was to distinguish from the first test
	# Ostree upgrade failed: mkdir(boot/loader.0/entries): Input/output error
	# Ostree upgrade failed: Error writing to file descriptor: No space left on device
	rm -f /var/bigfile
}

phase_check_update()
{
	ostree admin status

	local PHASE_DATA=$(get_phase_data_path)
	local COMMIT_BEFORE=$(cat $PHASE_DATA)

	# The current commit after the update
	local COMMIT_AFTER="$(ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p')"

	if [ "$COMMIT_BEFORE" = "$COMMIT_AFTER" ] ; then
		echo "The update did not apply, the current commit is the same: ok"
	else
		echo "The update applied, the current commit has changed ($COMMIT_BEFORE != $COMMIT_AFTER)"
		exit 1
	fi
}

update_manager_phases\
	phase_boot\
	phase_clean_previous_install\
	phase_test_update_does_not_start_if_disk_too_low\
	phase_update_stops_safely_if_disk_too_low\
	phase_check_update
